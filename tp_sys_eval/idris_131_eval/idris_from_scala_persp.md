### Explaining Idris and HoTT for someone inbound from Scala

Stub22 and others feel Idris is quite approachable, as a learning exercise,
for seasoned scala programmers.

Idris interfaces (+ impls) are akin to Scala traits (+ classes),
and are useful for plumbing and interoperability.  
This interface concept regulates the runtime model of Idris in analogy to the
Scala hybrid functional+object model.

Both systems support type dependence.

#### What ideas does Idris add to a modern toolset?

The main feel of Idris is weaving type dependencies into value function
recipes, in terse syntax, with support for formal proofs.  It is applicable
to compilers, content generators, rule engines, query planners, proof checkers,
protocol analyzers, test result comparators.  It also fits the objectives
of a certain open-sci-cosmo project.

In constraint, Idris type parameters work the same basic way as
scala generic types.  Key differences:

1. Idris syntax is denser, if less precise for engineering purposes.
2. Idris import model is simpler, more restrictive, nimbler for small projects.
3. Idris supports value-dependent types, and returning types from functions

Number 3 is the whole point of this exercise, but the other pieces are
also interesting, including the underlying haskell-ness.

### Will Idris last?  Can we run Idris-like inside Scala?  
Possible outcomes to anticipate:

    * Dependent types goes further mainstream
    * More CiC proof engines directly in Scala, for example based in [MMT](https://uniformal.github.io/) by Uniformal, [Kwarc.info](http://www.kwarc.info), Prof. Florian Rabe.
