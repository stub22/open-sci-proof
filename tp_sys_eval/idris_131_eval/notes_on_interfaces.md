## The top part of this note started as a compilation of quotes from the Idris tutorial on interfaces
http://docs.idris-lang.org/en/latest/tutorial/interfaces.html

```  
 interface Show shn where
   showIt : shn -> String
```
> This generates a function of the following type (which we call a method of
> the Show interface):

```showIt : Show shn => shn -> String```

> We can read this as: “under the constraint that shn [the type] has an 
> implementation of  Show, take an input a and return a String.” 
> An implementation of an interface is defined by giving definitions 
> of the methods of the interface.
> For example, the Show implementation for Nat could be defined as:

```
Show Nat where
    showIt Z = "Z"
    showIt (S k) = "s" ++ showIt k
```    

> Implementation declarations can themselves have constraints. 
> To help with resolution, the arguments of an implementation must be 
> constructors (either data or type constructors) or variables 
> (i.e. you cannot give an implementation for a function). 
> For example, to define a Show implementation for vectors, we need to know
>  that there is a Show implementation for the element type, because we are
> going to use it to convert each element to a String:
```
Show a => Show (Vect n a) where
    show xs = "[" ++ show' xs ++ "]" where
        show' : Vect n a -> String
        show' Nil        = ""
        show' (x :: Nil) = show x
        show' (x :: xs)  = show x ++ ", " ++ show' xs
```        
> Interfaces can also be extended. A logical next step from an equality 
> relation Eq is to define an ordering relation Ord. We can define an Ord 
> interface which inherits methods from Eq as well as defining some of its own:
```
data Ordering = LT | EQ | GT
interface Eq a => Ord a where
    compare : a -> a -> Ordering
```    

Meaty 2014 question (when "interfaces" were still "classes") with answer from E. Brady:

https://stackoverflow.com/questions/24837387/idris-function-to-construct-empty-list-a-where-a-is-bound-to-an-instance-of/24839309#24839309

> **Q:** Does this mean that interfaces are actually just syntactic sugar for data declarations?    
> **A. Yes.**
>
> **Q:** If interfaces are actually first class, then what's the deal with =>?
> 
> **A:** Interfaces are just a type, so when you write Num ty => ... that is 
> treated similar to {auto x : Num ty} -> ..., which means => is just a
> syntactic sugar that makes the interface argument an implicit argument

2018 discussion of a kind of interface setup that Idris won't instantiate,
with links to interesting Idris-dev issues in the comments.

https://stackoverflow.com/questions/50546623/in-idris-why-do-interface-parameters-have-to-be-type-or-data-constructors

Some reddit discussion of what interface syntax means in comparison to Haskell.

https://www.reddit.com/r/Idris/comments/8hgbem/what_are_interfaces/

Here is an example of using interfaces with tuples, from the Prelude/Interfaces.idr

```
(Ord a, Ord b) => Ord (a, b) where
  compare (xl, xr) (yl, yr) =
    if xl /= yl
      then compare xl yl
      else compare xr yr
```      

Multi-constraint example from state machines tutorial

http://docs.idris-lang.org/en/latest/st/machines.html

```getData : (ConsoleIO m, DataStore m) => ST m () []```

> Since we want to use methods of the DataStore interface, we’ll constraint the
> computation context m so that there must be an implementation of DataStore. 
> We also have a constraint ConsoleIO m so that we can display any data we 
> read from the store, or any error messages.