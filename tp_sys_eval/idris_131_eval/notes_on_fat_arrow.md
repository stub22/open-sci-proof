
# Known uses of Fat-Arrow => in Idris Lang

### Background, and comparison to Haskell
This symbol is called "fat arrow":   =>

Idris is partly based on Haskell.   Fat-arrow => is used in Haskell in the 
[definition of typeclasses](https://stackoverflow.com/questions/9142731/what-does-the-symbol-mean-in-haskell), 
which are comparable to Idris interfaces (section "A" below).

### Idris has three(3) flavors of =>

In Idris, fat arrows are used in 3 main ways:  interfaces, case patterns, and anonymous functions.

#### A) Interfaces
  * Problem:  Idris interface syntax is not defined under "Syntax Guide" in language reference (v 1.3.1)
  * ...but we know about them from this tutorial:

      * http://docs.idris-lang.org/en/latest/tutorial/interfaces.html
      * Our notes and snippets from that tutorial:        [notes_on_interfaces.md](notes_on_interfaces.md)
       
  * Places where fat-arrow is used to define/extend/implement interfaces:
    1. Generated form of interface method (Q: Is it legit src syntax, too?)
       * ```showIt : Show shwn => shwn -> String```  
       * Here "Show" is a interface name, "shwn" is a type which must have an explicit impl for Show.    
    2. Constraint of implementation.
       * Here the impl is for (Vect n shwn) as the (showable) argument type, while "Show shwn" is a constraint than shwn is also showable.
          * ```Show shwn => Show (Vect n shwn) where``` 
             * ```showIt vec = "[" ++ sfnc vec ++ "]" where ...```
    3. Extension (i.e. inheritance, subsumption, is-a).  Note here we are declaring Special, inheriting from existing General.
        * ``` interface General shwn => Special shwn where ... ```
   * In all three cases, ```lhs => rhs``` means lhs is a predicate necessary to match, "then" rhs can be defined/applied.
     
#### B) Case pattern match expressions
In Idris case syntx, the meaning of fat-arrow is conceptually similar to the interface usage.  Again, lhs is a predicate.
``` 
case <test> of
    <case 1> => <expr>
    <case 2> => <expr>
    ...
    otherwise => <expr>
```
   
#### C) Anonymous functions (lambdas), wherein the meaning diverges somewhat from the above A&B meanings.  
```
-- One argument lambda function
(\x => <expr>)
-- Two argument lambda function
(\x, y => <expr>)
```

