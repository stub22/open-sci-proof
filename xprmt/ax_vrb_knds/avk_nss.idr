
module AvkNss

-- %access export

export
aNat : Nat
aNat = 8

export
goodFunc : Int -> Int
goodFunc v = 2 * v

export
goodTxt : String
goodTxt = "so good 4 u"

namespace NsOne
  export
  anInt : Int
  anInt = 9
  export
  blankLinesOK : String
  blankLinesOK = "blanks OK"
aDoub : Double
aDoub = 7.5

-- Indenting these gives a syntax error about "not a terminator"
-- because the namespace NsOne is already closed by the non-indented
-- aDoub.  We think.
-- Currently not exported, so not visible by importer.
resumeIndent : String
resumeIndent = "resumed"

namespace NsTwo

  booger : Double
  booger = 4.4
  -- Indents matter
  gimlet : Int
  gimlet = -3

data Bunch = MkBunch String Int Double String

tbnch : Bunch
tbnch = MkBunch "furst-[" 2 3.3 "]-lest"

{-
https://github.com/idris-lang/Idris-dev/wiki/Idris-for-Haskellers
Top level functions must have type annotations.

Functions and data types must be defined in the source file before it can be used.
Mutually dependent functions can be linked using a mutual block.

fmap is map in Idris.

There is no !! infix operator to get an element from a list by its index,
use index function instead.

Record syntax has a slightly different syntax in Idris, notably, records
have only one constructor in Idris, unlike in Haskell, where records can
have multi constructors. Why this limitation ? Multi constructor records
have non-total accessor functions.

Unlike GHCI, the Idris REPL is not inside of an implicit IO monad.
Use :x in the REPL for executing IO actions.
-}
