## Axiomagic Verb Kinds - first try in Idris

Using [Idris](http://www.idris-lang.org) language (v 1.3.1)

This experiment defines some toy models of natural language verbs,
together with runnable Idris programs.

As a sub-problem, we are matching RDF-Quads modeled as key-value pairs.

  * Toy RQS is modeled as a key-value store, where 
    * key = {g,s,p}
    * value = bag of {o}
  * RQS store queries used as value generator feeding (web) service pipeline.
  * Downstream application decisions use pipeline as ground truth.
    * Total application is a function of state in a set of (actual + virtual) RQS stores.
    * Web UI uses virtual RQS store projected into each user's [Html, CSS, JSON] space.
    * Upstream models rely on typechecks, instance proofs, and small inline unit tests.
  * "Verb Kinds" = Example of hard problem, using proof support, leading towards cosmology (via epistemology, via ontology).
    * Assuming a large set of well defined nouns, define some verbs over those nouns.