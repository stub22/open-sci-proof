module AvkQnTup


{-  We avoid distinguishing QName as a Type, because so far just tuple-hacking.
  Instead we're making this qui k-n-dirty QnPrt, and tossing those into tuples.
  We are treating the prefix-abbr as simply a short snappy string.
  It would be possible to make malformed QName pairs with e.g. two prefixes.
-}

data QnPrt : Type where
    QnPrfAb : String -> QnPrt  -- prefix-abbrev part of qname, before ':', e.g. "xsd"
    QnLclNm : String -> QnPrt  -- local name part,  after the ':''

implementation Eq QnPrt where
      (QnPrfAb a) == (QnPrfAb b) = a == b
      (QnLclNm a) == (QnLclNm b) = a == b
      _ == _ = False

-- In sort ordering, prefixAb is "less than" LclNm
implementation Ord QnPrt where
      compare (QnPrfAb a) (QnPrfAb b) = compare a b
      compare (QnLclNm a) (QnLclNm b) = compare a b
      compare (QnPrfAb a) (QnLclNm b) = LT
      compare (QnLclNm a) (QnPrfAb b) = GT

makeQN : QnPrt -> QnPrt -> (QnPrt, QnPrt)
makeQN prfx lcl = (prfx, lcl)

cnctQN : (QnPrt, QnPrt) -> String -- dump the QN in a turtle-like string
cnctQN ((QnPrfAb pabb), (QnLclNm lcln)) = pabb ++ ":" ++ lcln

testQn1 : (QnPrt, QnPrt)
testQn1 = makeQN (QnPrfAb "pabt") (QnLclNm "wee_woow_so_local_yes")

testQn2 : (QnPrt, QnPrt)
testQn2 = makeQN (QnPrfAb "pabt") (QnLclNm "puddle_ponger")

-- This tagged tuple (3x2) could be a record or a list instead
-- Note the inner qnames are not tagged, and the QnPrts are.
data RQSBagKey = RQSBagKey_GSP ((QnPrt, QnPrt), (QnPrt, QnPrt), (QnPrt, QnPrt))

-- This all works, but there is little value in explicitly writing out the pairs.
-- MkKey_GS : (QnPrt, QnPrt) -> (QnPrt, QnPrt) -> ((QnPrt, QnPrt), (QnPrt, QnPrt))
-- MkKey_GS (gpa, gln) (spa, sln) = ((gpa, gln), (spa, sln))
-- MkKey_GSP : (QnPrt, QnPrt) -> (QnPrt, QnPrt) -> (QnPrt, QnPrt) -> ((QnPrt, QnPrt), (QnPrt, QnPrt), (QnPrt, QnPrt))
-- MkKey_GSP (gpa, gln) (spa, sln) (ppa, pln) = ((gpa, gln), (spa, sln), (ppa, pln))
-- MakeTuple3 gqn sqn pqn = (gqn, sqn, pqn)

bagKeyFromRaw : String -> String -> String -> String -> String -> String -> RQSBagKey
bagKeyFromRaw gpa gln spa sln ppa pln =
  let   grphQN = makeQN (QnPrfAb gpa) (QnLclNm gln)
        subjQN = makeQN (QnPrfAb spa) (QnLclNm sln)
        propQN = makeQN (QnPrfAb ppa) (QnLclNm pln)
        gspKTpl = (grphQN, subjQN, propQN)
  in RQSBagKey_GSP gspKTpl



-- Let's make a sextuple (Yeah Beavis, make it real fast).
-- But ... comparing these seems to be hard work for Idris!
qprtTp6 : RQSBagKey -> (QnPrt, QnPrt, QnPrt, QnPrt, QnPrt, QnPrt)
qprtTp6 (RQSBagKey_GSP ((gpa, gln), (spa, sln), (ppa, pln))) = (gpa, gln, spa, sln, ppa, pln)

qprtLst : RQSBagKey -> List QnPrt
qprtLst (RQSBagKey_GSP ((gpa, gln), (spa, sln), (ppa, pln))) = [gpa, gln, spa, sln, ppa, pln]

-- Including this makes compile take a long time (30+ sec?) and makes .ibc file big.
-- cmpViaTup6 : RQSBagKey -> RQSBagKey -> Ordering
-- cmpViaTup6 a b = compare (qprtTp6 a) (qprtTp6 b)

-- Note these naive comparisons are RDF-correct only if all RQSBagKeys are using
-- same prefix abbreviations.

eqViaLst : RQSBagKey -> RQSBagKey -> Bool
eqViaLst a b = (qprtLst a) == (qprtLst b)

cmpViaLst : RQSBagKey -> RQSBagKey -> Ordering
cmpViaLst a b = compare (qprtLst a) (qprtLst b)

  -- compare aTp6 bTp6
-- qprtTp3x2 : RQSBagKey -> ((QnPrt, QnPrt), (QnPrt, QnPrt), (QnPrt, QnPrt))

-- qprtVect6 : RQSBagKey -> Vect 6 QnPrt

implementation Eq RQSBagKey where
  bk1 == bk2 = eqViaLst bk1 bk2

implementation Ord RQSBagKey where
  compare a b = cmpViaLst a b

implementation Show RQSBagKey where
  show (RQSBagKey_GSP (grphQN, subjQN, propQN)) =
    let   gtxt = cnctQN grphQN
          stxt = cnctQN subjQN
          ptxt = cnctQN propQN
    in "[G=(" ++ gtxt ++ "),S=("++ stxt ++"),P=("++ ptxt ++")]"

someKeys : List RQSBagKey
someKeys =
    let   ky1 = bagKeyFromRaw "xtg" "good_grph" "dmi" "xcool_cat" "dmt" "has_tail"
          ky2 = bagKeyFromRaw "xtg" "weird_grph" "dmi" "cool_cat" "dmt" "has_fur"
          ky3 = bagKeyFromRaw "xtg" "good_grph" "dmi" "hot_dog" "dmt" "has_bark"
    in [ky1, ky2, ky3]

sk2 : RQSBagKey
sk2 = index 2 someKeys
