-- Hey Wow
{-
http://docs.idris-lang.org/en/latest/tutorial/modules.html

An Idris program consists of a collection of modules.
A file can start with a module kewyord.  Module names can be hierarchical:
module Semantics.Transform
Each file can define only a single module, which includes everything defined in that file.

There is no formal link between the module name and its filename, although
it is generally advisable to use the same name for each.
An import statement refers to a filename, using dots to separate directories.
For example, import foo.bar would import the file foo/bar.idr, which would
conventionally have the module declaration module foo.bar.
The only requirement for module names is that the main module, with the
main function, must be called Main — although its filename need not be Main.idr.
-}
||| Doc for this sweet module
module Main   -- Executable program must define a module called "Main"

{-
Idris allows for functions, types, and interfaces to be marked as:
private, export, or public export. Their general meaning is as follows:
private meaning that it’s not exported at all. This is the default.
export meaning that its top level type is exported.
public export meaning that the entire definition is exported.
-}
-- https://github.com/idris-lang/Idris-dev/blob/master/libs/base/Data/Vect.idr
-- An import makes the names in another module available for use by the current module:
-- "All the declarations in an imported module are available for use in the file."
-- Should say all the EXPORTED declarations, right?
-- Note that names made visible by import are not, by default, re-exported to
-- users of the module being written. This can be done using import public:

import Data.Vect  -- Import from Idris Libs
-- Seems to resolve using file name, but symbols imported using module name
-- regardless of "as"
import avk_nss
import avk_num_expr
import avk_pnt
import avk_qntup

%default total    -- a directive

{-
https://github.com/idris-lang/Idris-dev/wiki/Windows-Binaries

Adding idris and idris/mingw/bin folders to PATH is a good idea if you plan
to follow the official tutorial.
-}
foo : Nat         -- a declaration
foo = 5

-- idt : Double
-- idt = NsTwo.booger -- AvkNss.NsTwo.booger

stxt : String
stxt = goodTxt

otxt : String
-- Here and below in ltxt, we access this item at different ns levels
otxt = AvkNss.NsOne.blankLinesOK

notResumed : String
-- This only works if resumeIndent was exported by the AvkNss module.
-- notResumed = resumeIndent

ltxt : List String
ltxt = ["gt=" ++ goodTxt, "blok1=" ++ blankLinesOK, "nob=" ++ NsOne.blankLinesOK]

hdr : String
hdr = "First test main for Axmc Verb Kinds.  Enter something! "

-- So far this IO works in most console setups tried.
main : IO ()
main = do
    -- All statements in the do block must start in same column
    putStrLn hdr
    smthng <- getLine
    let resp = "Resp=Entered: " ++ smthng
    putStrLn (resp)
    putStrLn ("Enter something else")
    anudder <- getLine
    putStrLn ("Got second thing: " ++ anudder)
    putStrLn ("Bye-Bye!")
