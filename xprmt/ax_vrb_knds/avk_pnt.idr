
module AvkPnt

-- import Data.Vect

someTxt : String
someTxt = "heyDere-"

-- Start with kushy interior part of RDF, in the fully resolved URI and Lit.
data RdfLit innrLit = RLit innrLit
data RdfUri = RUri String

-- Now we must define RDF Nodes.
-- All nodes are XNodes or BNodes.
-- XNode = xplicit node, suitable for use in "object" of quad.
-- Is either a URI or a typed literal.
-- We must declare or imply a favLitType, even for Uri case.
-- But value of favLitTV is only used in RXL case.
data RdfXNode favLitTV = RXU RdfUri | RXL (RdfLit favLitTV)

-- BNode sketch defined, but not yet available in Quad.
data RdfBNode locName = RBN String -- locName is a local id, not serialized or queried

-- Next we can define a quad statement as either RQZ-ero, or RQS g s p o
-- This quad does not yet support BNodes, in any position.
data RdfQuadStmt fltv = RQZ | RQS RdfUri RdfUri RdfUri (RdfXNode fltv)

someUris : (RdfUri, RdfUri, RdfUri)
someUris = (RUri "urn:tstGrph", RUri "urn:tstSbj", RUri "urn:tstPrp")

-- Within a quad, the triple (g, s, p) is "key", while o is always "value".
rli_19 : RdfLit Int
rli_19 = RLit 19

xri_19 : RdfXNode Int
xri_19 = RXL rli_19

li_A : List Int
li_A = [14, 16, 225, -6, 19, 14, -18]

lrli_A : List (RdfLit Int)
lrli_A = map (\iv => RLit iv) li_A

qd_RQZ : RdfQuadStmt Void
qd_RQZ = RQZ

qd_B1 : RdfQuadStmt Int
qd_B1 = RQS (fst someUris) (fst  someUris) (fst someUris) xri_19

interface RQuadUser rqut where
  useRQ : rqut -> RdfQuadStmt fltv -> (Bool, String)

interface RQDescriber rqdt where
  descRQ : rqdt -> RdfQuadStmt Int -> String

interface Fake faker where
  fazool : faker -> String

-- It seems the keyword "implementation" is optional.
-- Are there differences in the exact meaning with and without?
implementation Fake Nat where
  fazool n = "Faking fazool order=" ++ (show n)

implementation RQDescriber Nat where
  descRQ n rqsInt = "Describing RQ in style=" ++ (show n)

implementation RQuadUser Nat where
  useRQ n rqAny = (True, "Using RQ at level=" ++ (show n))

nat_5 : Nat
nat_5 = 5

tstOut_Nat : String
tstOut_Nat =
  let   tn1 = fazool nat_5        -- String
        tn2 = descRQ nat_5 qd_B1  -- String
        tn3 = useRQ nat_5 qd_RQZ  -- (Bool, String)
  in "[tn1={" ++ tn1 ++ "}, tn2={" ++ tn2 ++ "}, tn3={" ++ (show tn3) ++ "}]"

data MyChooser = MyC String

implementation Fake MyChooser where
  fazool (MyC ccd) = "Chzr-Fazool using code=[" ++ ccd ++ "]"

implementation RQDescriber MyChooser where
  descRQ (MyC ccd) rqsInt = "Chzr-Desc using=[" ++ ccd ++ "]"

implementation RQuadUser MyChooser where
  useRQ (MyC ccd) rqAny = (True, "Chzr uses RQ via=[" ++ ccd ++ "]")

agchzr : MyChooser
agchzr = MyC "{gd chzr koad here}"

t1d : String
t1d = descRQ agchzr qd_B1

tstOut_MyC : String
tstOut_MyC =
  let   tn1 = fazool agchzr        -- String
        tn2 = descRQ agchzr qd_B1  -- String
        tn3 = useRQ agchzr qd_RQZ  -- (Bool, String)
  in "[tn1={" ++ tn1 ++ "}, tn2={" ++ tn2 ++ "}, tn3={" ++ (show tn3) ++ "}]"

-- TODO: mark this as an assertion fail
-- t2d : String
-- t2d = descRQ agchzr qd_RQZ
-- t3d : (Bool, String)
-- t3d = useRQ agchzr qd_B1

showRQ  : (RdfQuadStmt idxLVT) -> String
showRQ RQZ = "found RQ-Zero"
showRQ (RQS g s p o) = "found RQuad G=" ++ ".gnosho."

t30_out : String
t30_out =
  let   zout = showRQ qd_RQZ
        vout = showRQ qd_B1
  in "[zout={" ++ zout ++ "}, vout={" ++ vout ++ "}]"

  {- RDF quads give us unordered collection.
  g s p v    is another way to write a quad, using v for value (instead of o).
  In Idris, we may represent quad using QNames (+ prefixes during export).
  Then we may sort a quad collection using the string-orders of (g, s, p) QNames.
  Gives natural partition into our dataset for each graph, subjs, and props(+vals).
  It further sorts and x-parts by qNames of the form pa:bareName, in each ordering

  Predicate filters match quads for reaping into buckets, which remain sorted.
  The sorting allows us to access some simple quad patterns pretty easily,
  and is hoped to perform OK for small jobs under Idris v1.3.1.
  -}

srtdStmtQdLst: prfVT -> List (RdfQuadStmt prfVT)


--RQuadFilter MyChooser where
--  acceptRQS RQZ = (True, "Nada") -- We gladly accept nothing
  -- acceptRQS RQS g s p o = (True, "Accepting: G=" ++ show g ++ "S=" ++  show s )

-- data RQuadIter
-- interface RQStore
--    getAllQuads

-- Fun syntax example from WP
data Tree : Type -> Type where
    Node : Tree a -> Tree a -> Tree a
    Leaf : a -> Tree a

-- EOF
