
module AvkExpr
-- Started from Haskell example on
-- https://en.wikipedia.org/wiki/Generalized_algebraic_data_type

%default total

-- Idris full GADT syntax defines the type-cons as a function into Type,
-- whereas some Haskell GADT typeclass can use just "data OurExpr a where...""
data OurExpr : a -> Type where
    -- each Constructor
    EBool  : Bool     -> OurExpr Bool
    EInt   : Int      -> OurExpr Int
    EEqual : OurExpr Int -> OurExpr Int  -> OurExpr Bool
    EAny : anyType -> OurExpr anyType

ourEval : OurExpr a -> a

ourEval e = case e of
    EBool a    => a
    EInt a     => a
    EEqual a b => (ourEval a) == (ourEval b)
    EAny x => x

{-
Here OEW is intended to mean "empty wrapper"
-}
data OurWrap eit =  OEW | OGW eit | OBW eit

anEmptyWrap : OurWrap Int
anEmptyWrap = OEW

aFullWrap : OurWrap Double
aFullWrap = OGW 7.5

anExprWrap : OurWrap (OurExpr Double)
anExprWrap = OGW (EAny 4.2)

partial
unwrap : OurWrap xtyp -> xtyp
unwrap xv = case xv of
    -- OEW : OurType Void => void
    OGW xpr => xpr
    OBW xpr => xpr

partial
uoe : OurWrap (OurExpr Double) -> Double
uoe (OGW (EAny dv)) = 5.5 * dv


data IdrMaybe : (a : Type) -> Type where
    ||| No value stored
    IdrNothing : IdrMaybe a
    ||| A value of type `a` is stored
    IdrJust : (x : a) -> IdrMaybe a

makeNothing : nt -> IdrMaybe nt
makeNothing nv = IdrNothing

nd : IdrMaybe Double
nd = makeNothing 3.0

makeNoDouble :  IdrMaybe Double
makeNoDouble = IdrNothing

nndd : IdrMaybe Double
nndd = makeNoDouble

data BtrWrap : wvt  -> Type where
    BWE : BtrWrap wvt
    BWF : wv -> BtrWrap wv

-- Machine int is Int, while Integer is a bignum

eiw : BtrWrap Int
eiw = BWE

fiw : BtrWrap Int
fiw = BWF 7

someInts : (Int, Int, Int, Int)
someInts = (4, 9, -5, 7)

anIL : List Int
anIL = [5, 33, -9, 812]

someDbs : (Double, Double, Double, Double)
someDbs = (2.8, -7.4, 125.283, 0.0006)
i5 : Int
i5 = 5
other5 : Int
other5 = i5
d22 : Double
d22 = 2.2

-- i6, i7 : Int
-- i6 = 6
-- i7 = 7

sd : Double
sd = fst someDbs

somePair : (String, Double)
somePair = ("yeah", 18.94)

{-
partial
makeBW : vvv -> BtrWrap vvv
makeBW i5 = BWE
makeBW d22 = BWF d22
-}
-- makeBW (fst someDbs) = BWE
-- makeBW (fst someInts) = BWF 100
-- makeBW (fst (snd someDbs)) = BWF 100.0

-- someBWs : List (BtrWrap Integer)
-- someBWs = let x1 = (5 in [makeBW x1]
-- last line
