module AvkQdStr

data QuadStore = QSA String | QSB | QSN

data QuintReg = QntReg ((quintKey_opt : (Maybe String))  -> QuadStore)

implementation Show QuintReg where
  show (QntReg qntKeyToQdStr) =
    let   q5Txt = "qntMppr55"
    in "[Q5=(" ++ q5Txt ++ ")]"

tstQdQnt : QuintReg
tstQdQnt =
  let
    dfltQdStr = QSB
    dfltQuintReg = QntReg (\qko => dfltQdStr)
  in dfltQuintReg


kiwiAppNm : String
kiwiAppNm = "kiwi_app"
grapeAppNm : String
grapeAppNm = "grape_app"
grapeQdQnt : QuintReg
grapeQdQnt =
  let
    gdfltStr = QSN
    qdStr1 = QSB
    qdStr2 = QSA "yep"
    rgMpFunc =  (\qko => case qko of
        Nothing => gdfltStr
        Just "store_41" => qdStr1
        Just "store_42" => qdStr2)
    grpQntRg = QntReg rgMpFunc
  in grpQntRg

makeKiwiQnt : QuintReg
makeKiwiQnt =
  let
    kdfltStr = QSN
    kiwiStrs = [QSA "store65", QSA "store_66", QSA "store67"]
    -- kstrs = map  kiwiStrNms
    -- let fn_nmIdx_maybe = (\nm => elemIndex nm kiwStrNms)
    rgMpFunc  = (\qko => case qko of
        Nothing => kdfltStr
        Just nm => QSA nm )
    kiwQntRg = QntReg rgMpFunc
  in kiwQntRg


DtFnc6 : (sxtKey_opt : (Maybe String))  -> QuintReg
DtFnc6 Nothing = tstQdQnt
DtFnc6 (Just grapeAppNm) = grapeQdQnt

tstLvl6 : String
tstLvl6 =
  let
    app100 = DtFnc6 Nothing
    app200 = DtFnc6 (Just grapeAppNm)
    app300 = DtFnc6 (Just kiwiAppNm)
    lstOfApps = [app100, app200, app300]
  in show lstOfApps

fiveIsFive : 5 = 5
-- fiveIsFive = Refl

twoPlusTwo : 2 + 2 = 4
-- twoPlusTwo = Refl
