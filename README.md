# open-sci-proof - Applications of Theorem Proving and Symbolic Calculation

This open science project contains notes on applications of symbolic calculation and theorem proving. 
* 2020-Aug:  Blog post on [Type Theory for Knowledge Graphs](https://outrect.blogspot.com/2020/07/stu-b22-shares-2020-summer-reading-list.html)
* 2019-Mar:  Using Symja + Scala + Javascript for symbolic + numeric evalation + display of Euler-Lagrange diffeqs.
* 2019-Feb:  Dependent type experiments using [Idris language](https://www.idris-lang.org), for programs and proofs.
Idris is a strong-typed descendant of Agda, Haskell, and ML, using a condensed syntax, and featuring dependent types.
Idris is intended to be usable for both progamming software and proving theorems, which is exactly our intersection of interest in this project.     

#### Useful starting links
* [Idris-Lang website](http://www.idris-lang.org) and accompanying [docs](http://docs.idris-lang.org)
* Idris [core language feature list](http://docs.idris-lang.org/en/latest/reference/language-features.html)
 
## Type Theory Background
To whet our appetite, first we review the idea of [GADT](https://en.wikipedia.org/wiki/Generalized_algebraic_data_type),
in functional programming.  Here GADT stands for General Algebraic Data Type.

Next, we observe how in recent decades, type theory has grown in importance to human thinking about
foundations of math, computer science, and the symbolic side of AI.  Plenty
of info is available in WikiPedia+, on both math and computing sides,
starting from: [Martin-Löf](https://en.wikipedia.org/wiki/Intuitionistic_type_theory) 
and [Curry-Howard](https://en.wikipedia.org/wiki/Curry-Howard_correspondence).

Sprinting then to the deep end of the pool, one can peruse the lovely HoTT book: [Homotopy Type Theory](https://homotopytypetheory.org/book/).

It is available as free PDF and has some sections approachable for any reader.

From printed page 11 (PDF page 23):

<img src="_img_quot/hott_bk/hott_compare_table_01.png"/>

We invite you to check out the beautiful Table of Contents, at the very least.

If you find type theory interesting, and you already know one of Haskell, Scala, F#, Clojure, 
then you might give Idris a try.

### The O-S-P project - scope of software side
  * Overall design pursues experimental flexibility, supported by rigor.  
  (Adopting the ideas of proof assistants community incl. Coq, Agda, Lean)  
    * (Almost) all assumptions are explicit and can be toggled + tweaked.
    * Example apps: Lagrangian models for [science](https://gitlab.com/stub22/open-sci-cosmo) and [financial optimization](https://gitlab.com/stub22/open-fin-onto)
* [Lagrangian eval prototype](https://github.com/stub22/axiomagic/tree/master/adaxmvn/axmgc_dmo_fibo/src/main/scala/axmgc/xpr/sym_mth), partly compatible with Wolfram / Mathematica.  Open-source Scala + Javascript.
  * [SymCalc2019](https://gitlab.com/stub22/open-sci-proof/blob/master/sym_calc/SymCalc2019.md) - our roundup of relevant specs and compatible tools
  * [Symja parser+eval setup](./sym_calc/SymjaParserSetup.md) - notes on our config, ymmv
* [RDF pipe mapper](./xprmt/ax_vrb_knds) in Idris.
  * Logical + Functional model of RDF embedded into Idris.
  * RQS stores feed value generators feeding (web) service pipelines.
  * Downstream application decisions use pipeline as ground truth.
    * Total application is a function of state in a set of (actual + virtual) RQS stores.
    * Web UI uses virtual RQS store projected into each user's [Html, CSS, JSON] space.
    * Upstream models rely on typechecks, instance proofs, and small inline unit tests.
* Demo goal:  "Verb Kinds" = Example of hard problem, using proof support.
    * Assuming a large set of well defined nouns, define useful verbs over those nouns.

### Applications of our model + proof technique

#### Open Astrophysics:  Fitting galactic rotation curves to axiomatic models

* See our sibling project on gitLab:  [open-sci-cosmo](https://gitlab.com/stub22/open-sci-cosmo/blob/master/osc_model_2019/osc_model_terms.md). 
  * [Challenge C](https://gitlab.com/stub22/open-sci-cosmo/blob/master/osc_model_2019/osc_model_README.md):  Sketch brackets on assumptions that allow conventional calculations to proceed with an estimated accuracy.
    * Inquiry:  What categories of model (axiom brackets) give the same answers (numeric brackets)?
* o-s-Cosmo application relies on proof checking of models.
  * Prototype models described in Idris, to be published via RQS pipeline.
  * Long term (2-3 yrs) possibility: Idris eventually is not (type/proof/calc-) strong enough for our cosmo model.
    * Then: revisit [Lean](http://leanprover.github.io), [Agda](https://agda.readthedocs.io), 
Uniformal's [MMT](https://uniformal.github.io/), [Coq](https://coq.inria.fr/), [Isabelle/HOL](https://isabelle.in.tum.de/).

#### Open Finance: Estimating option prices

 * [Open Fin Onto](https://gitlab.com/stub22/open-fin-onto) project intro.
   * [Subproject on option pricing](https://gitlab.com/stub22/open-fin-onto/blob/master/option_price/README_option_price.md)


### Getting Started (for non-Haskell people)
Idris binaries are sufficient for running, testing, compiling, so it is not 
necessary to set up a Haskell env, if you don't already have one.  
(This fact is quite appealing to stub22 and maybe others).

[Idris binaries - Windows](https://github.com/idris-lang/Idris-dev/wiki/Windows-Binaries)

[Idris distros - all platforms](https://github.com/idris-lang/Idris-dev/wiki/Installation-Instructions)

@stub22 is testing with the 64 bit idris.exe (v 1.3.1) on MS-Windows 7 and 10.  
Linux binaries and other runtime options are available, see Idris-lang website.

### Arriving at Idris from Haskell

Idris is built in Haskell and shares some syntax with it.  

Idris 1.x core is implemented via Haskell projects, and can be treated as an
loadable extension via cabal and hackage.  

Here are [key differences between Idris and Haskell](https://github.com/idris-lang/Idris-dev/wiki/Idris-for-Haskellers)

Idris has dependent type model and proof checker at its core, like Agda before it.
We are calling these systems Strong-Typed FP.
Idris, Haskell, Scala, F# and others all provide elements of Strong-Typed FP.
Idris is arguably the cleanest synthesis, at the **language** level, of advanced
theoretical concepts with a good dose of pragmatism.  The influences of these
other systems are many.  

#### Our notes on specific aspects of Idris
* [Comparing uses of Idris Fat-Arrow](tp_sys_eval/idris_131_eval/notes_on_fat_arrow.md
)
* [Notes on Idris Interfaces](tp_sys_eval/idris_131_eval/notes_on_interfaces.md)

### Contrasting Idris with Scala, other FP notes

* [notes relating Idris world to Scala](tp_sys_eval/idris_131_eval/idris_from_scala_persp.md)
* [FP for beginners](./FP_Tutorial.md)

