# SymCalc 2019 
## Symbolic Calculation Choices in Axiomagic, O.S.P., O.F.O. projects.

* Symbolic algebra + calculus stacks are available for all major compute platforms. 
* Many systems display increasing compatibility with Wolfram Mathematica language (MMA).
* Formalization trend:  Semantic convergence towards OpenMath and MathML-content.

### Specifications
* [OpenMath Tech Overview](https://www.openmath.org/technical/) - Formal dictionaries of math constructs.
  * [OpenMath.js Javascript API Ref](https://lurchmath.github.io/openmath-js/site/api-reference/) = A good way to get a feeling for the schema.
  * [OpenMath-JSON](https://omjson.kwarc.info) - OpenMath encoded as JSON (with JSON schema).  
* [MathML](https://www.w3.org/Math/) - W3 standards for Web publishing with semantic annotations.  May include OpenMath fragments and references.
* [WLTools](https://wltools.github.io/LanguageSpec/) = An unofficial crowdsourced specification of the Wolfram language used in Mathematica
  * [List-of-W-Language-Projects/](https://wltools.github.io/LanguageSpec/Resources/List-of-W-Language-Projects/) - overlaps with tools listed below.
* [Rubi](https://rulebasedintegration.org/) integration ruleset - open source integration rules that work with Mathematica and some other programs, including Symja and Expreduce.

## Open Source Tools of interest, grouped by platform

### Impls: Java / Scala
* [Symja](https://github.com/axkr/symja_android_library) - (also goes by an older name: Matheclipse)
  * Long-standing integrated, interactive math system for both Java VM and Android
    * [Web Demo](http://matheclipse.org/) - experiment here in easygoing "relaxed" syntax mode.  (Also available: stricter Mathematica-like mode)
  * Integrates several crunchy java libraries with an interactive parser and output formatters for Tex and MathML (+ SVG plots, etc.).
    * Includes Rubi integration ruleset.
    * Uses [JAS](http://krum.rz.uni-mannheim.de/jas/) Java Algebra System
    * Includes [Hipparchus](https://www.hipparchus.org/) and [LogicNG](https://github.com/logic-ng/LogicNG)
  * Our detailed setup notes here [SymjaParserSetup](./SymjaParserSetup.md)
  * Potentially usable in streaming use cases (Spark, Kafka), investigation ongoing in Spring 2019.
* [FoxySheep](https://github.com/rljacobson/FoxySheep) = Wolfram Language parser (using ANTLR) with generators for Java, Python. 
* [SymJava](https://github.com/yuemingl/SymJava) = "fast symbolic-numeric computation...Java bytecode is generated at runtime for symbolic expressions which make the numerical evaluation really fast."
* [omath](https://github.com/omath/omath) = "...should be able to run many simple Mathematica programs."
* [Scala parser for Mathematica language](https://github.com/mattpap/mathematica-parser)
* [Mathematica plugin for IntelliJ IDEA](https://wlplugin.halirutan.de/) - Helps us write Mathematica-__compatible__ exprs, parsable in WL-compatible tools like Symja, FoxySheep, Expreduce, SymPy-Gamma 

### Impls: C / C++
* [SymEngine](https://github.com/symengine) native engine with wrappers for Julia, Python, R, Ruby, Haskell

### Impls: Julia 
* [SymEngine.jl](https://github.com/symengine/SymEngine.jl) wrapper for Julia

### Impls: Go
* [WL parser](https://godoc.org/modernc.org/wl) - Wolfram Language parser impl in golang, comparable to FoxySheep.
* [Expreduce](https://github.com/corywalker/expreduce) independent algebra impl in golang, uses WL parser
  * Includes Rubi integration rules 

### Impls: Python
* [SymPy](http://sympy.org/) - Well known symbolic calc library, can use SymEngine
  * [Calculus module](https://docs.sympy.org/latest/modules/calculus/index.html) includes direct support for Euler-Lagrange differential eqns. 
* [Sage](http://www.sagemath.org) - Popular application using SymPy and other tools
* [SymPy Gamma](https://github.com/sympy/sympy_gamma/) = "A SymPy version of Wolfram Alpha".  
  * [Online Interactive Demo](https://sympygamma.com/)
* [Mathics](https://mathics.github.io/) - Mathematica-like interaction using SymPy

### Impls: Javascript
 * [MathJS](https://mathjs.org/docs/reference/functions.html) - Widely used evaluation lib, has some limited symbolic capabilities, derivatives, matrix ops.
 * [Nerdamer](https://nerdamer.com/) - Has more symbolic features, including integration
 * [OpenMath.js](https://lurchmath.github.io/openmath-js/) - create + access openMath definitions from JavaScript
 * [MathJax](https://www.mathjax.org/) - Renders equations in any web browser, using input in MathML, TeX, AsciiMath formats.
 * [jqMath](https://mathscribe.com/author/jqmath.html) - Lighter alternative (to MathJax) for equation rendering