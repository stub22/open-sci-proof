# Symja Parser Setup notes

### Prerequisites
* JDK 8

## Embedding a Symja parser + evaluator in Java, Scala, or other JVM language
* Our example code uses Scala (2.12), built with maven.
  * Test code folder in "Axiomagic" project on GitHub:  https://github.com/stub22/axiomagic/tree/master/adaxmvn/axmgc_dmo_fibo/src/main/scala/axmgc/xpr/sym_mth
* Symja API Options
  * Mid Level - our current focus, using EvalEngine and ExprParser
  * Lower level - use EvalEngine.parseNode and  AST2Expr.CONST.convert
  * Higher level - use ExprEvaluator, includes more output features and config assumptions

Symja supports two(+) related input syntaxes, controlled by some configuration
flags.

 * Parsing modes:  
   * *relaxed* mode (mostly case-insensitive) 
   * *MMA-like* mode (mostly case-sensitive)
   * Choice determined by boolean "relaxedSyntax" flag passed to EvalEngine constructor.
 * Important global flag:  `Config.PARSER_USE_LOWERCASE_SYMBOLS`
   * Globalness means all parsers must use same value of this flag. 
   * Default value is true, meaning LOWERCASE mode is active.  
   * Flag is used in 75 places in 31 files of Symja codebase (source + tests)
   * When using MMA mode (relaxed == false), it is customary to set `Config.PARSER_USE_LOWERCASE_SYMBOLS = false`
     * Symja example code for MathML and TeX output uses this MMA combo:  relaxed == false, Config.PARSER_USE_LOWERCASE_SYMBOLS == false.
     * With this combination (only!), the symja parser behavior appears generally close to the WL un-spec.  ("Cos[1.0]" works but "cos[1.0]" does not.  Yay!)
     * If we __don't__ set `PARSER_USE_LOWERCASE_SYMBOLS = false`, then in MMA-like (unrelaxed) mode, the default LOWERCASE-true behavior contradicts Symja and MMA docs.  ("cos[1.0]" works but "Cos[1.0]" does not.  Boo!)
 * Symja docs regarding consequences of these flags can be improved.  (TODO:  Help with this!)