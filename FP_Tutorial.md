

### Bienvenidos 

Do you program in C#, Java, Javascript, Python, R?

Purist functional programming is a worthwhile discipline to understand, just
as purist data typing and formal proof are good disciplines.  We like Idris as
a learning enviornment for becoming a better programmer and scientist.  It
can also be practically effective in dense modeling, for apps discussed above.
Most concepts that we learn for Clojure, F#, Scala, Haskell, Idris and other 
FPs are compatible.  The biggest differences (for beginners) are just syntax.

The fun part of Strong-Typed FP is the clean, precise logical modeling 
of anything describable with functions and types.  

Some tricky parts are mapping to outputs, and interop with stateful systems.